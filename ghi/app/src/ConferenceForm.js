import React, { useState, useEffect } from 'react'

function ConferenceForm () {

    const [locations, setLocations] = useState([])
    const [name, setName] = useState()
    const [start, setStart] = useState()
    const [end, setEnd] = useState()
    const [description, setDescription] = useState()
    const [presentation, setPresentation] = useState()
    const [attendee, setAttendee] = useState()
    const [location, setLocation] = useState()

    const fetchData = async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
    }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value)
    }

    const handleStartChange = (e) => {
        const value = e.target.value;
        setStart(value)
    }

    const handleEndChange = (e) => {
        const value = e.target.value;
        setEnd(value)
    }

    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value)
    }

    const handlePresentationChange = (e) => {
        const value = e.target.value;
        setPresentation(value)
    }

    const handleAttendeeChange = (e) => {
        const value = e.target.value;
        setAttendee(value)
    }

    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};

        data.name = name
        data.starts = start
        data.ends = end
        data.description = description
        data.max_presentations = presentation
        data.max_attendees = attendee
        data.location = location

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference)

        setName()
        setStart()
        setEnd()
        setDescription()
        setPresentation()
        setAttendee()
        setLocation([])
    }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} placeholder="startdate" type="date" id="starts" name="starts" className="form-control" />
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} placeholder="end" type="date" id="ends" name="ends" className="form-control" />
                <label htmlFor="ends">End date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} placeholder="description" type="textarea" id="description" name="description" className="form-control">
                </textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentationChange} placeholder="max_presentations" type="number" id="max_presentations" name="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeeChange} placeholder="max_attendees" type="number" id="max_attendees" name="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                    <option key={location.href} value={location.id}>
                        {location.name}
                    </option>
                    )
                  })};
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default ConferenceForm
