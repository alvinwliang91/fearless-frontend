import React, { useState, useEffect } from 'react'

function PresentationForm () {

    const [conferences, setConferences] = useState([])


    function useDefaultState() {
        const [state, setState] = useState("")
        function handleChange(event) {
            event.preventDefault()
            setState(event.target.value)
        }
        function reset() {
        	setState("")
    	}
  	return {"state": state, "handleChange": handleChange, "reset": reset}
    }


    const presenterName = useDefaultState()
    const companyName = useDefaultState()
    const title = useDefaultState()
    const synopsis = useDefaultState()
    const conference = useDefaultState()



    const fetchData = async () => {

        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences)
          }
        }

        useEffect(() => {
            fetchData();
        }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {}

        data.presenter_name = presenterName.state
        data.company_name = companyName.state
        data.title = title.state
        data.synopsis = synopsis.state
        data.conference = conference.state
        console.log(data)



        const presentationUrl = `http://localhost:8000/api/conferences/${conference.state}/presentations/`
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
          const newPresentation = await response.json();
            presenterName.reset()
            companyName.reset()
            title.reset()
            synopsis.reset()
            conference.reset()


        }
    }




    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={presenterName.handleChange} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={companyName.handleChange} placeholder="startdate" type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="starts">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={title.handleChange} placeholder="title" type="text" id="title" name="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={synopsis.handleChange} placeholder="synopsis" type="textarea" id="synopsis" name="synopsis" className="form-control">
                </textarea>
              </div>
              <div className="mb-3">
                <select onChange={conference.handleChange} name="conference" id="conference" className="form-select" required>
                  <option value="">Choose a Conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default PresentationForm
