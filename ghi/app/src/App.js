import logo from './logo.svg';
import React, { useState, useEffect } from 'react'
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './Attend-Conference';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';


function App(props) {
  const [ conferences, setConferences ] = useState([]);

  async function getConferences() {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    getConferences();
  }, []);

  if (props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />

      <Routes>
      <Route index element={<MainPage conferences={conferences} />} />
        <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
            <Route index element={<AttendeesList attendees={props.attendees} />} />
        </Route>
        <Route path="conferences">
            <Route path ="new" element={<ConferenceForm />}></Route>
        </Route>
        <Route path="locations">
            <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}



export default App;
